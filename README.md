Overview
This notebook provides the script for the FHN bacteria model which was reported in Stratford et al., PNAS (2019).

In this study, we analyzed bacterial membrane potential dynamics when stimulated by an electric stimulus. The paper reported that proliferative cells and inhibited cells repond in apparent oppsite directions to an electrical stimulus. This discovery presented a new way of rapidly detecting live bacterial cells, which led to the foundation of a University spin-out company, Cytecom Ltd.

To gain a mechanistic insight how an identical stimulus gives rise to different responses, we built the FHN bacteria model. The model demonstrated that a shift in resting membrane potential can give rise to the observed opposite dynamics. Note that the model is adapted from the FHN neuron model.

For more details, please see the publication. 
https://doi.org/10.1073/pnas.1901788116

Munehiro Asally